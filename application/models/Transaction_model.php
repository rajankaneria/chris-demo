<?php 

class Transaction_model extends CI_Model{

    public function sentHistory($email){
		$query = $this->db->query("select * from payment where payer='$email' order by id desc");
		$result = $query->result_array();
		return $result;
	}

    public function recivedHistory($email){
		$query = $this->db->query("select * from payment where payee='$email' order by id desc");
		$result = $query->result_array();
		return $result;
	}

    public function sendMoney($to,$amount){
        $this->load->model("user_model");
        $from = $this->session->userdata('email');
        $userRow = $this->user_model->userRow($from);
        if($userRow["balance"]>=$amount){
            $this->db->where('email', $from);
            $this->db->set('balance', 'balance-'.$amount, FALSE);
            $this->db->update('account');

            $this->db->where('email', $to);
            $this->db->set('balance', 'balance+'.$amount, FALSE);
            $this->db->update('account');

            $transactionData = array(
                "payer" => $from,
                "payee" => $to,
                "amount" => $amount
            );
            $this->db->insert('payment',$transactionData);
            $output = "success";
        }else{
            $output = "fail";
        }
        return $output;
    }

}