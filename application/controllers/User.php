<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function signup()
	{
		$userData = $this->input->post("userData");
        $email = $userData["email"];
		$password = md5($userData["password"]);
		$card = $userData["card"];

		$errorMessage = array();
		$this->load->model("user_model");
		//check if emai is already registerd
		$emailCount = $this->user_model->emailExistCheck($email);
		//check if card is already used
		$cardCount = $this->user_model->cardExistCheck($card);

		if($emailCount == 0 && $cardCount==0){
			//register user
			$this->user_model->signup($userData);
			$output = array(
				"status" => "success",
				"message" => "Successfully registered"
			);
			$this->session->set_userdata('email', $email);
		}else{
			//send error message
			$output = array(
				"status" => "failed",
				"message" => "Email or Card already exist"
			);
		}
		echo json_encode($output);
	}

	public function login(){
		$this->load->model("user_model");
		$userData = $this->input->post("userData");
		$loginCheck = $this->user_model->loginCheck($userData);
		if($loginCheck == 1){
			$output = array(
				"status" => "success",
				"message" => "Successfully Logged in"
			);
			$this->session->set_userdata('email', $userData["email"]);
		}else{
			$output = array(
				"status" => "failed",
				"message" => "Invalid Email or password"
			);
		}
		echo json_encode($output);
	}

	public function sendMoney(){
		$to = $this->input->post("to");
		$amount = $this->input->post("amount");
		$this->load->model("transaction_model");
		$result = $this->transaction_model->sendMoney($to,$amount);
		$output["status"] = $result;
		echo json_encode($output);
	}

	public function logout(){
		$this->session->unset_userdata('email');
		$this->session->sess_destroy();
		redirect('login', 'refresh');
	}

}
