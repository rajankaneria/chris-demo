<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		if($this->session->has_userdata('email')){
			redirect('/home', 'refresh');
		}else{
			$viewData = array();
			$templateData = array(
				"view" => "login",
				"viewData" => $viewData
			);
			$this->load->view('template',$templateData);
		}
	}

}
