<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->has_userdata('email')){
			$this->load->model("user_model");
			$this->load->model("transaction_model");
			$userRow = $this->user_model->userRow($this->session->userdata('email'));
			$sentHistory = $this->transaction_model->sentHistory($this->session->userdata('email'));
			$recivedHistory = $this->transaction_model->recivedHistory($this->session->userdata('email'));
			$viewData = array(
				"userRow"=>$userRow,
				"sentHistory"=>$sentHistory,
				"recivedHistory"=>$recivedHistory
			);
			$templateData = array(
				"view" => "dashboard",
				"viewData" => $viewData
			);
			$this->load->view('template',$templateData);
		}else{
			redirect('/home', 'refresh');
		}
	}
}
