<?php $this->load->view("includes/fixed-navbar"); ?>
<div class="container">
    <div class="row">
        <div class="col s12 m8 l6 login-container">
            <div class="card-panel card-container white">
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs blue darken-4">
                            <li class="tab col s6"><a class="active" href="#loginPanel">Login</a></li>
                            <li class="tab col s6"><a href="#signupPanel">Signup</a></li>
                        </ul>
                    </div>
                    <div id="loginPanel" class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="yourname@xyz.com" id="email" type="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s12">
                                <input placeholder="xxxxxxxxx" id="password" type="password" class="validate">
                                <label for="password">Password</label>
                            </div>
                            <div class="input-field col s12">
                                <a id="loginBtn" class="waves-effect waves-light btn btn-full blue darken-4">Login</a>
                            </div>
                        </div>
                    </div>
                    <div id="signupPanel" class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="eg: 123456789" id="card" type="text" class="validate">
                                <label for="text">Prepaid Card</label>
                            </div>
                            <div class="input-field col s12">
                                <input placeholder="yourname@xyz.com" id="email" type="email" class="validate">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s12">
                                <input placeholder="xxxxxxxxx" id="password" type="password" class="validate">
                                <label for="password">Password</label>
                            </div>
                            <div class="input-field col s12">
                                <a id="signupBtn" class="waves-effect waves-light btn btn-full blue darken-4">Signup</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

