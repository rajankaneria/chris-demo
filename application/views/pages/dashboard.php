<?php $this->load->view("includes/navbar",$userRow); ?>

<div class="container">
    <div class="row">
        <div class="col s12 m8 l8 login-container">
            <div id="sendMoneyContainer" class="card-panel card-container white">
                <div class="row blue darken-4 center-align card-panel-title">Send Money</div>
                <div class="row send-money-container">
                    <div class="input-field col s12 m5 l5">
                        <input placeholder="yourname@xyz.com" id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12 m3 l3">
                        <input placeholder="in USD" id="amount" type="text" class="validate">
                        <label for="amount">Amount ($)</label>
                    </div>
                    <div class="input-field col s12 m4 l4">
                        <a id="sendMoney" class="waves-effect waves-light btn btn-full blue darken-4">Send</a>
                    </div>
                </div>
            </div>
            <div class="card-panel card-container white">
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs blue darken-4">
                            <li class="tab col s6"><a class="active" href="#sentPanel">Sent</a></li>
                            <li class="tab col s6"><a href="#recivedPanel">Recived</a></li>
                        </ul>
                    </div>
                    <div id="sentPanel" class="col s12">
                        <div class="row">
                            <?php if(sizeof($sentHistory)>0){ ?>
                            <table>
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <th class="right-align">Amount</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach($sentHistory as $historyRow){ ?>
                                <tr>
                                    <td><?php echo $historyRow["payee"]; ?></td>
                                    <td class="right-align">$<?php echo $historyRow["amount"]; ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php }else{ ?>
                            <p class="no-record-message">No records found</p>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="recivedPanel" class="col s12">
                        <div class="row">
                            <?php if(sizeof($recivedHistory)>0){ ?>
                            <table>
                                <thead>
                                <tr>
                                    <th>Email</th>
                                    <th class="right-align">Amount</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach($recivedHistory as $historyRow){ ?>
                                <tr>
                                    <td><?php echo $historyRow["payer"]; ?></td>
                                    <td class="right-align">$<?php echo $historyRow["amount"]; ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php }else{ ?>
                            <p class="no-record-message">No records found</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

