

<!-- Modal Structure -->
  <div id="siteMessage" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4 id="modalTitle">Login Error</h4>
      <p id="modalText">Invalid login credentials. Please check your email or password.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
    </div>
  </div>


<!--
  <footer class="page-footer black">
    <div class="footer-copyright black">
      <div class="container center-align">
      © 2017 Chris-Demo
      </div>
    </div>
  </footer>
-->

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
          
  <script src="https://use.fontawesome.com/99c5739a85.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
  <?php foreach ($pageJS as $key => $fileName) { ?>
    <script src="<?php echo base_url(); ?>assets/js/<?php echo $fileName; ?>"></script>
  <?php } ?>
  <input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>" />

  </body>
</html>