        <!-- Nav Bar : start -->
        <div class="section">
          <div class="row">
            <div class="col s12 m12">
              <div class="site-logo"><a href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>html/images/ilmsquare.png" /></a></div>
              <div class="nav-bar">
                <a href="<?php echo base_url(); ?>home">Home</a>
                <a href="<?php echo base_url(); ?>teachers">Find a teacher</a>
                <a href="<?php echo base_url(); ?>teach">Teach</a>
                <a href="<?php echo base_url(); ?>about">About</a>
              </div>
            </div>
          </div>
        </div>
        <!-- Nav Bar : end -->