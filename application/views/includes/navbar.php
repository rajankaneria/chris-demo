<div class="navbar-fixed">
    <nav class="blue darken-4">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Logo</a>
            <ul id="nav-mobile" class="right">
                <li><a class="dropdown-button" href="#!" data-activates="dropdown1">$<?php echo $balance; ?></a></li>
            </ul>
        </div>
    </nav>
</div>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="<?php echo site_url() ?>/user/logout">Logout</a></li>
</ul>