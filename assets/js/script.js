var baseURL;
$(function(){
    baseURL = $("#baseUrl").val();
    $("#signupBtn").on("click",function(){
        signup();
    });
    $("#loginBtn").on("click",function(){
        login();
    });
    $("#sendMoney").on("click",function(){
        sendMoney();
    });
});

function signup(){
    var container = $("#signupPanel");
    var email = $("#email",container).val();
    var password = $("#password",container).val();
    var card = $("#card",container).val();
    var userData = {
        "email" : email,
        "password" : password,
        "card" : card
    };
    var errorMessage;
    var successFlag = true;
    //check if email is valid
    if(!validateEmail(userData.email)){
        errorMessage = "Enter a valid email address";
        successFlag = false;
    }
    //check if card is numeric
    if(isNaN(userData.card)){
        errorMessage = "Enter a valid card number";
        successFlag = false;
    }
    //check if any of the value is empty
    if(userData.email == "" || userData.password == "" || userData.card == ""){
        errorMessage = "All fields are required";
        successFlag = false;
    }
    if(successFlag){
        $.post(baseURL+"user/signup",{userData:userData},function(data){
            data = $.parseJSON(data);
            if(data.status == "success"){
                window.location.href = baseURL+"home";
            }else{
                siteMessage("Signup error",data.message);
            }
            
        });
    }else{
        siteMessage("Signup error",errorMessage);
    }
    
}

function login(){
    var container = $("#loginPanel");
    var email = $("#email",container).val();
    var password = $("#password",container).val();
    var userData = {
        "email" : email,
        "password" : password
    };
    $.post(baseURL+"user/login",{userData:userData},function(data){
        data = $.parseJSON(data);
        if(data.status == "success"){
            window.location.href = baseURL+"home";
        }else{
            siteMessage("Login Error",data.message);
        }
    });
}

function sendMoney(){
    var container = $("#sendMoneyContainer");
    var email = $("#email",container).val();
    var amount = $("#amount",container).val();

    if(!validateEmail(email)){
        siteMessage("Transaction error","Please enter a valid email address");
    }else if(isNaN(amount) || amount<=0){
        siteMessage("Transaction error","Please enter a valid amount");
    }else{
        $.post(baseURL+"user/sendMoney",{to:email,amount:amount},function(data){
            data = $.parseJSON(data);
            if(data.status == "success"){
                window.location.reload();
            }else{
                siteMessage("Transaction error","You do not have sufficient funds in the account.");
            }
        });
    }
}

function siteMessage(title,message){
    $("#siteMessage #modalTitle").html(title);
    $("#siteMessage #modalText").html(message);
    $('#siteMessage').modal('open');
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}